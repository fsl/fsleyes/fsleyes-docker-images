FSLeyes docker images
=====================

This repository contains a set of Dockerfiles which define Docker images that
are used for FSLeyes-specific testing and building. These images are available
on the Docker hub at https://hub.docker.com/u/pauldmccarthy/


To build/update an image, follow these steps:


```sh
cd fsleyes-py36-wxpy4-gtk3
docker build -t pauldmccarthy/fsleyes-py36-wxpy4-gtk3 -f Dockerfile ..
docker login
docker push pauldmccarthy/fsleyes-py36-wxpy4-gtk3
```

**Important:** The `py27`, `py34`, `py35`, and `gtk2` images are no longer
maintained - they were last built at commit 5ac3e515.


The projects which use these docker images are hosted at:

 - https://git.fmrib.ox.ac.uk/fsl/fslpy/
 - https://git.fmrib.ox.ac.uk/fsl/fsleyes/widgets/
 - https://git.fmrib.ox.ac.uk/fsl/fsleyes/props/
 - https://git.fmrib.ox.ac.uk/fsl/fsleyes/fsleyes/
