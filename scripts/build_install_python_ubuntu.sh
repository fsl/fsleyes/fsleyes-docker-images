#!/bin/bash

set -e

pushd /
wget http://python.org/ftp/python/"$PYTHON_VERSION"/Python-"$PYTHON_VERSION".tar.xz
tar xf Python-"$PYTHON_VERSION".tar.xz
cd Python-"$PYTHON_VERSION"
./configure --prefix=`pwd`/install --enable-shared
make
make install
export PATH=`pwd`/install/bin:$PATH
export LD_LIBRARY_PATH=`pwd`/install/lib


# Make  sure that a command called "python" exists
pushd install/bin
if [ ! -f python ]; then
  ln -s python3 python;
fi

# get pip
wget https://bootstrap.pypa.io/get-pip.py
python get-pip.py

# Make sure that a command called "pip" exists
if  [ ! -f pip ]; then
  ln -s pip3.7 pip;
fi
popd
popd
