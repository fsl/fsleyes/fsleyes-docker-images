#!/bin/bash

set -e

apt-get install -y libgconf2-dev
apt-get install -y build-essential \
                   libgtk2.0-dev \
                   libwebkitgtk-dev \
                   libjpeg-dev \
                   libtiff-dev \
                   libsdl-dev \
                   libgstreamer0.10-dev \
                   libgstreamer-plugins-base0.10-dev \
                   libnotify-dev \
                   freeglut3-dev

/bin/bash /scripts/build_install_wxpython4.sh "$WXPYTHON_VERSION"

apt-get purge --auto-remove -y \
        freeglut3-dev \
        libasound2-dev \
        libasprintf-dev \
        libatk1.0-dev \
        libcaca-dev \
        libcairo2-dev \
        libdrm-dev \
        libfontconfig1-dev \
        libfreetype6-dev \
        libgdk-pixbuf2.0-dev \
        libgettextpo-dev \
        libgl1-mesa-dev \
        libglu1-mesa-dev \
        libgstreamer-plugins-base0.10-dev \
        libgstreamer0.10-dev \
        libgtk2.0-dev \
        libharfbuzz-dev \
        libice-dev \
        libicu-dev \
        libjavascriptcoregtk-1.0-dev \
        libjbig-dev \
        libjpeg-dev \
        libjpeg-turbo8-dev \
        libjpeg8-dev \
        liblzma-dev \
        libnotify-dev \
        libpango1.0-dev \
        libpixman-1-dev \
        libpng12-dev \
        libpthread-stubs0-dev \
        libpulse-dev \
        libsdl1.2-dev \
        libslang2-dev \
        libsm-dev \
        libsoup2.4-dev \
        libtiff5-dev \
        libwebkitgtk-dev \
        libx11-dev \
        libx11-xcb-dev \
        libxau-dev \
        libxcb-dri2-0-dev \
        libxcb-dri3-dev \
        libxcb-glx0-dev \
        libxcb-present-dev \
        libxcb-randr0-dev \
        libxcb-render0-dev \
        libxcb-shape0-dev \
        libxcb-shm0-dev \
        libxcb-sync-dev \
        libxcb-xfixes0-dev \
        libxcb1-dev \
        libxcomposite-dev \
        libxcursor-dev \
        libxdamage-dev \
        libxdmcp-dev \
        libxext-dev \
        libxfixes-dev \
        libxft-dev \
        libxi-dev \
        libxinerama-dev \
        libxml2-dev \
        libxrandr-dev \
        libxrender-dev \
        libxshmfence-dev \
        libxt-dev \
        libxxf86vm-dev \
        mesa-common-dev \
        x11proto-composite-dev \
        x11proto-core-dev \
        x11proto-damage-dev \
        x11proto-dri2-dev \
        x11proto-fixes-dev \
        x11proto-gl-dev \
        x11proto-input-dev \
        x11proto-kb-dev \
        x11proto-randr-dev \
        x11proto-render-dev \
        x11proto-xext-dev \
        x11proto-xf86vidmode-dev \
        x11proto-xinerama-dev \
        xtrans-dev

rm -rf /var/lib/apt/lists/*
