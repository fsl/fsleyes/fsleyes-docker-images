#!/bin/bash

set -e

if [[ $CENTOS_VERSION = '8'* ]]; then

  sed -ie "s/^#baseurl.*$/baseurl=http:\/\/vault.centos.org\/$CENTOS_VERSION\/BaseOS\/x86_64\/os/g"    /etc/yum.repos.d/CentOS-AppStream.repo
  sed -ie "s/^#baseurl.*$/baseurl=http:\/\/vault.centos.org\/$CENTOS_VERSION\/AppStream\/x86_64\/os/g" /etc/yum.repos.d/CentOS-Base.repo
  sed -ie "s/^#baseurl.*$/baseurl=http:\/\/vault.centos.org\/$CENTOS_VERSION\/extras\/x86_64\/os/g"    /etc/yum.repos.d/CentOS-Extras.repo
  sed -ie "s/^mirrorlist.*$/# nomirror/g" /etc/yum.repos.d/CentOS-AppStream.repo
  sed -ie "s/^mirrorlist.*$/# nomirror/g" /etc/yum.repos.d/CentOS-Base.repo
  sed -ie "s/^mirrorlist.*$/# nomirror/g" /etc/yum.repos.d/CentOS-Extras.repo

else

  sed -ie "s/^#baseurl.*\/os\/.*$/baseurl=http:\/\/vault.centos.org\/$CENTOS_VERSION\/os\/x86_64/g"                 /etc/yum.repos.d/CentOS-Base.repo
  sed -ie "s/^#baseurl.*\/extras\/.*$/baseurl=http:\/\/vault.centos.org\/$CENTOS_VERSION\/extras\/x86_64/g"         /etc/yum.repos.d/CentOS-Base.repo
  sed -ie "s/^#baseurl.*\/updates\/.*$/baseurl=http:\/\/vault.centos.org\/$CENTOS_VERSION\/updates\/x86_64/g"       /etc/yum.repos.d/CentOS-Base.repo
  sed -ie "s/^#baseurl.*\/centosplus\/.*$/baseurl=http:\/\/vault.centos.org\/$CENTOS_VERSION\/centosplus\/x86_64/g" /etc/yum.repos.d/CentOS-Base.repo
  sed -ie "s/^mirrorlist.*$/# nomirror/g" /etc/yum.repos.d/CentOS-Base.repo

fi

yum clean all
