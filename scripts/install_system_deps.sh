#!/bin/bash

set -e

# Install system dependencies
apt-get update  -y
apt-get install -y --ignore-missing \
  bc \
  software-properties-common \
  python-software-properties \
  xvfb \
  libgtk2.0-0 \
  libnotify4 \
  freeglut3 \
  libsdl1.2debian \
  bzip2 \
  wget \
  rsync \
  git \
  openssh-client

cat /etc/lsb-release | grep "14.04" && apt-get install -y libspatialindex-c3   || true
cat /etc/lsb-release | grep "16.04" && apt-get install -y libspatialindex-c4v5 || true
cat /etc/lsb-release | grep "18.04" && apt-get install -y libspatialindex-c4v5 || true

# "pip install rtree" doesn't seem to
# lookup libspatialindex correctly
pushd /usr/lib/x86_64-linux-gnu/ > /dev/null

lsi=`ldconfig -p | grep libspatialindex.so | tr -d '\t' | cut -d ' ' -f 1`
lsi_c=`ldconfig -p | grep libspatialindex_c.so | tr -d '\t' | cut -d ' ' -f 1`

ln -s "$lsi"   libspatialindex.so
ln -s "$lsi_c" libspatialindex_c.so

popd > /dev/null
