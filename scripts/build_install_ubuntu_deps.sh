#!/bin/bash

set -e

# Install system dependencies
apt-get install -y software-properties-common || true
apt-get install -y python-software-properties || true
apt-get install -y --ignore-missing \
  bc \
  build-essential \
  xvfb \
  libgtk2.0-0 \
  libgtk-3-0 \
  libnotify4 \
  freeglut3 \
  libsdl1.2debian \
  libgtk-3-dev \
  libreadline-gplv2-dev \
  libncursesw5-dev \
  libssl-dev \
  libsqlite3-dev \
  tk-dev \
  libgdbm-dev \
  libc6-dev \
  libbz2-dev \
  libffi-dev \
  zlib1g \
  wget \
  rsync \
  git \
  openssh-client \
  libosmesa6

# if compiling wxpython from source,
# add dev dependencies
if [[ -z "$WXPYTHON_URL" ]]; then
  apt-get install -y libgconf2-dev
  apt-get install -y dpkg-dev \
          build-essential \
          freeglut3-dev \
          libgl1-mesa-dev \
          libglu1-mesa-dev \
          libgstreamer-plugins-base1.0-dev \
          libgtk-3-dev \
          libjpeg-dev \
          libnotify-dev \
          libpng-dev \
          libsdl2-dev \
          libsm-dev \
          libtiff-dev \
          libwebkit2gtk-4.0-dev \
          libxtst-dev

fi

cat /etc/lsb-release | grep "16.04" && apt-get install -y libsdl2-2.0-0

cat /etc/lsb-release | grep "14.04" && apt-get install -y libspatialindex-c3   || true
cat /etc/lsb-release | grep "16.04" && apt-get install -y libspatialindex-c4v5 || true
cat /etc/lsb-release | grep "18.04" && apt-get install -y libspatialindex-c4v5 || true
cat /etc/lsb-release | grep "20.04" && apt-get install -y libspatialindex-c6   || true

# "pip install rtree" doesn't seem to
# lookup libspatialindex correctly
pushd /usr/lib/x86_64-linux-gnu/ > /dev/null

lsi=`ldconfig -p | grep libspatialindex.so | tr -d '\t' | cut -d ' ' -f 1`
lsi_c=`ldconfig -p | grep libspatialindex_c.so | tr -d '\t' | cut -d ' ' -f 1`

ln -s "$lsi"   libspatialindex.so
ln -s "$lsi_c" libspatialindex_c.so

popd > /dev/null
