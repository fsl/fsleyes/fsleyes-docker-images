#!/bin/bash
# Download, compile, and install python

set -e

pushd /
wget http://python.org/ftp/python/"$PYTHON_VERSION"/Python-"$PYTHON_VERSION".tar.xz
tar xf Python-"$PYTHON_VERSION".tar.xz
cd Python-"$PYTHON_VERSION"


# we have to use a hand-installed openssl on
# centos 6 (see build_install_centos_deps.sh)
# https://benad.me/blog/2018/07/17/python-3.7-on-centos-6/
# https://gist.github.com/AlmogCohen/e9e0667e875f955e0d4783b7433ed604
if [[ `cat /etc/centos-release | grep "release 6."` ]]; then
  sed -i 's/SSL=\/usr\/local\/ssl/SSL=\/usr\/local\/openssl11/g' Modules/Setup.dist
  sed -i '211,214 s/^##*//' Modules/Setup.dist
  export LDFLAGS="-Wl,-rpath=/usr/local/openssl11/lib"
  CONFIG_FLAGS="--with-openssl=/usr/local/openssl11/"
fi

./configure --prefix=`pwd`/install --enable-shared $CONFIG_FLAGS
make
make install
export PATH=`pwd`/install/bin:$PATH
export LD_LIBRARY_PATH=`pwd`/install/lib


# Make  sure that a command called "python" exists
pushd install/bin
if [ ! -f python ]; then
  ln -s python3 python;
fi

# get pip
wget https://bootstrap.pypa.io/get-pip.py
python get-pip.py

# Make sure that a command called "pip" exists
if  [ ! -f pip ]; then
  ln -s pip3.7 pip;
fi
popd
popd
