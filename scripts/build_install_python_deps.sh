#!/bin/bash

set -e

$PY_VENV /build.venv
source /build.venv/bin/activate
pip install --upgrade pip setuptools

# pre-built binaries for wxpython are available
# for some, but not for others, where it needs
# to be compiled from source. If the WXPYTHON_URL
# variable is set, we assume it points to a
# pre-compiled wheel URL. If unset, we compile
# from source.
if [[ -z "$WXPYTHON_URL" ]]; then
  chmod a+x /scripts/build_install_wxpython4.sh
  /scripts/build_install_wxpython4.sh "$WXPYTHON_VERSION" "$GTK_VERSION"
else
  pip install --only-binary wxpython -f "$WXPYTHON_URL" wxpython=="$WXPYTHON_VERSION"
fi

# Other standard dependencies
pip install \
    pyinstaller \
    numpy \
    scipy \
    matplotlib \
    pyparsing \
    jinja2 \
    pillow \
    nibabel \
    indexed_gzip \
    xnat \
    wxnatpy \
    trimesh \
    rtree \
    pyzmq \
    tornado \
    ipykernel \
    ipython \
    jupyter-client \
    notebook \
    six \
    sphinx \
    sphinx-rtd-theme \
    pytest \
    pytest-cov \
    mock \
    coverage \
    pylint \
    flake8

# Installing pyopengl-accelerete the
# standard way doesn't seem to work -
# the numpy module doesn't get installed.
# Force-installing from source works
# though.
pip install --no-binary ":all:" "pyopengl>=3.1.0" "pyopengl-accelerate>=3.1.0"
