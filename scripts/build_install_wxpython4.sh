#!/bin/bash

set -e

WXPYTHON_VERSION=$1
GTK_VERSION=$2

pip install requests numpy six Pillow

mkdir $VIRTUAL_ENV/wx-build
pushd $VIRTUAL_ENV/wx-build > /dev/null

git config --global url.git://github.com/.insteadOf git@github.com/
git config --global url.git://github.com/.insteadOf https://github.com/

git clone git://github.com/wxWidgets/Phoenix.git
pushd Phoenix > /dev/null
git checkout wxPython-$WXPYTHON_VERSION
git submodule update --init --recursive


# patch the wxwidgets build opts
configopts="            '--disable-webviewwebkit',
            '--disable-webview',
            '--enable-graphics_ctx',
            '--enable-display',
            '--enable-geometry',
            '--enable-debug_flag',
            '--enable-optimise',
            '--disable-debugreport',
            '--enable-uiactionsim',
            '--enable-autoidman',
            '--with-xtest']"

startline=$(cat buildtools/build_wxwidgets.py | grep -n "wxpy_configure_opts =")
startline=$(echo -n $startline | sed -e 's/^\([0-9]\+\).*$/\1/g')

endline=$(tail -n +"$startline" buildtools/build_wxwidgets.py | grep -n -e "^ *\] *$")
endline=$(echo -n $endline | sed -e 's/^\([0-9]\+\).*$/\1/g')
endline=$(echo "$startline + $endline" | bc)

head -n "$startline" buildtools/build_wxwidgets.py  > tmp
echo "$configopts"                                 >> tmp
tail -n +"$endline" buildtools/build_wxwidgets.py  >> tmp
mv tmp buildtools/build_wxwidgets.py

sed -ie "s/^\(.*html2.*\)$/#\1/g" wscript

# As of wxPython 4.0.0, build.py forces the
# use of C++11, even though none of the
# wxwidgets/phoenix uses any C++ features.
sed -ie "s/'-std=c++11'/''/g" build.py

# python setup.py install will attempt
# to re-run the build we are about to
# do manually, so we need to patch
# setup.py
sed -ie "s/'install' *: wx_install/'install' : orig_install/g" setup.py

# The bundled/precompiled version of
# doxygen is too old for centos6
if [[ `cat /etc/centos-release | grep "release 6."` ]]; then
  git clone git://github.com/doxygen/doxygen.git
  pushd doxygen
  git checkout Release_1_8_8
  ./configure
  make
  mv bin/doxygen ../bin/doxygen-1.8.8
  popd
  export DOXYGEN=$(pwd)/bin/doxygen-1.8.8
  rm -rf doxygen
fi

# do the build
python ./build.py dox etg --nodoc sip build --release --"$GTK_VERSION"

python setup.py install --skip-build

# sometimes we get files which are rw-------
chmod -R a+rx $VIRTUAL_ENV/lib/

popd > /dev/null
popd > /dev/null
