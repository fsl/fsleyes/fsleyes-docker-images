#!/bin/bash

set -e

if [[ `cat /etc/centos-release | grep "release 6."` ]]; then
  CENTOS_VERSION=6
elif [[ `cat /etc/centos-release | grep "release 7."` ]]; then
  CENTOS_VERSION=7
elif [[ `cat /etc/centos-release | grep "release 8."` ]]; then
  CENTOS_VERSION=8
else
  echo "Cannot determine centos version"
  exit 1
fi

# default to gtk2 if GTK_VERSION is unset
if [[ -z "$GTK_VERSION" ]]; then
    GTK_VERSION="gtk2"
fi

yum check-update -y || true

# Install compilers and dependencies
# for compiling Python, and
# fsleyes/wxpython runtime dependencies
yum groupinstall -y "Development tools"
yum install --nogpgcheck -y \
    bc \
    wget \
    which \
    tar \
    zlib-devel \
    bzip2-devel \
    openssl-devel \
    ncurses-devel \
    sqlite-devel \
    readline-devel \
    gdbm-devel \
    xz-devel \
    expat-devel \
    libffi-devel \
    freeglut \
    libnotify \
    mesa-libGLU \
    mesa-libOSMesa \
    mesa-dri-drivers \
    SDL \
    xorg-x11-server-Xvfb \
    "$GTK_VERSION"-devel \
    libjpeg-turbo-devel \
    libtiff-devel \
    libXtst-devel \
    SDL-devel \
    libSM-devel \
    libnotify-devel \
    freeglut-devel

if [ "$CENTOS_VERSION" == "8" ]; then
  yum install --nogpgcheck -y \
    gstreamer1-devel \
    gstreamer1-plugins-base-devel
else
  yum install -y \
    gstreamer-devel \
    gstreamer-plugins-base-devel \
    libpcap-devel
fi

# openssl libs on centos 6 are too old to
# compile python 3.7 against
# https://benad.me/blog/2018/07/17/python-3.7-on-centos-6/
if [ "$CENTOS_VERSION" == "6" ]; then
 cd /tmp
 wget 'https://www.openssl.org/source/openssl-1.1.0h.tar.gz'
 tar -xf openssl-1.1.0h.tar.gz
 cd openssl-1.1.0h
 ./config shared --prefix=/usr/local/openssl11 --openssldir=/usr/local/openssl11
 make
 make install
fi

# libspatialindex is not
# available on centos
mkdir /spatialindex_build
pushd /spatialindex_build
wget http://download.osgeo.org/libspatialindex/spatialindex-src-1.8.5.tar.bz2
tar xf spatialindex-src-1.8.5.tar.bz2
pushd spatialindex-src-1.8.5
./configure
make
make install
echo "/usr/local/lib" > /etc/ld.so.conf.d/usrlocal.conf
ldconfig -v
popd
popd

# "pip install rtree" doesn't seem to
# lookup libspatialindex correctly
pushd /usr/lib64/ > /dev/null

lsi=`ldconfig -p | grep libspatialindex.so | tr -d '\t' | cut -d ' ' -f 1`
lsi_c=`ldconfig -p | grep libspatialindex_c.so | tr -d '\t' | cut -d ' ' -f 1`

ln -s "$lsi"   libspatialindex.so
ln -s "$lsi_c" libspatialindex_c.so

popd > /dev/null
